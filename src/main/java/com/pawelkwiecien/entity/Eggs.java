package com.pawelkwiecien.entity;

import javax.persistence.*;

@Entity
@Table(name = "eggs")
public class Eggs {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "eggs_total")
    private int eggsTotal;

    @Column(name = "eggs_incubation")
    private int eggsIncubation;

    @Column(name = "eggs_misshapen")
    private int eggsMisshapen;

    @Column(name = "eggs_double_yolk")
    private int eggsDoubleYolk;

    @Column(name = "eggs_cracked")
    private int eggsCracked;

    @Column(name = "eggs_weight")
    private double eggsWeight;

    @OneToOne(mappedBy = "eggs", cascade = CascadeType.ALL)
    private Entry entry;


    public Eggs(int eggsTotal, int eggsIncubation, int eggsMisshapen, int eggsDoubleYolk, int eggsCracked, double eggsWeight) {
        this.eggsTotal = eggsTotal;
        this.eggsIncubation = eggsIncubation;
        this.eggsMisshapen = eggsMisshapen;
        this.eggsDoubleYolk = eggsDoubleYolk;
        this.eggsCracked = eggsCracked;
        this.eggsWeight = eggsWeight;
    }

    public Eggs() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEggsTotal() {
        return eggsTotal;
    }

    public void setEggsTotal(int eggsTotal) {
        this.eggsTotal = eggsTotal;
    }

    public int getEggsIncubation() {
        return eggsIncubation;
    }

    public void setEggsIncubation(int eggsIncubation) {
        this.eggsIncubation = eggsIncubation;
    }

    public int getEggsMisshapen() {
        return eggsMisshapen;
    }

    public void setEggsMisshapen(int eggsMisshapen) {
        this.eggsMisshapen = eggsMisshapen;
    }

    public int getEggsDoubleYolk() {
        return eggsDoubleYolk;
    }

    public void setEggsDoubleYolk(int eggsDoubleYolk) {
        this.eggsDoubleYolk = eggsDoubleYolk;
    }

    public int getEggsCracked() {
        return eggsCracked;
    }

    public void setEggsCracked(int eggsCracked) {
        this.eggsCracked = eggsCracked;
    }

    public double getEggsWeight() {
        return eggsWeight;
    }

    public void setEggsWeight(double eggsWeight) {
        this.eggsWeight = eggsWeight;
    }

    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }

}
