package com.pawelkwiecien.entity;

import javax.persistence.*;

@Entity
@Table(name = "female_dosage_target")
public class FemaleDosageTarget {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "age_days")
    private int ageDays;
    @Column(name = "target")
    private double target;

    public FemaleDosageTarget(double target) {
        this.target = target;
    }

    public FemaleDosageTarget() {
    }

    public int getAgeDays() {
        return ageDays;
    }

    public void setAgeDays(int ageDays) {
        this.ageDays = ageDays;
    }

    public double getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }
}
