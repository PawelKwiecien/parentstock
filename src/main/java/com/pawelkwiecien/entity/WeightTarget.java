package com.pawelkwiecien.entity;

import javax.persistence.*;

@Entity
@Table(name = "weight_target")
public class WeightTarget {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "age_week")
    private int ageWeek;
    @Column(name = "target")
    private int target;

    public WeightTarget(int target) {
        this.target = target;
    }

    public WeightTarget() {
    }

    public int getAgeWeek() {
        return ageWeek;
    }

    public void setAgeWeek(int ageWeek) {
        this.ageWeek = ageWeek;
    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }
}
