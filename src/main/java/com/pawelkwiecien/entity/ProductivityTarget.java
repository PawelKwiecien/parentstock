package com.pawelkwiecien.entity;

import javax.persistence.*;

@Entity
@Table(name = "productivity_target")
public class ProductivityTarget {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "age_days")
    private int ageDays;

    @Column(name = "target")
    private double target;

    public ProductivityTarget(double target) {
        this.target = target;
    }

    public ProductivityTarget() {
    }

    public int getAgeDays() {
        return ageDays;
    }

    public void setAgeDays(int ageDays) {
        this.ageDays = ageDays;
    }

    public double getTarget() {
        return target;
    }

    public void setTarget(double target) {
        this.target = target;
    }
}
