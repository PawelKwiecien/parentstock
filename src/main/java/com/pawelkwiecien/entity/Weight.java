package com.pawelkwiecien.entity;

import javax.persistence.*;

@Entity
@Table(name = "weight")
public class Weight {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "house_id")
    private int houseId;

    @Column(name = "week_number")
    private int weekNumber;

    @Column(name = "weight_females")
    private int weightFemales;

    @Column(name = "weight_males")
    private int weightMales;


    public Weight(int houseId, int weekNumber, int weightFemales, int weightMales) {
        this.houseId = houseId;
        this.weekNumber = weekNumber;
        this.weightFemales = weightFemales;
        this.weightMales = weightMales;
    }

    public Weight() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHouseId() {
        return houseId;
    }

    public void setHouseId(int houseId) {
        this.houseId = houseId;
    }

    public int getWeekNumber() {
        return weekNumber;
    }

    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }

    public int getWeightFemales() {
        return weightFemales;
    }

    public void setWeightFemales(int weightFemales) {
        this.weightFemales = weightFemales;
    }

    public int getWeightMales() {
        return weightMales;
    }

    public void setWeightMales(int weightMales) {
        this.weightMales = weightMales;
    }

}
