package com.pawelkwiecien.entity;

import javax.persistence.*;

@Entity
@Table(name = "silo")
public class Silo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "feed_reserve_females")
    private double feedReserveFemales;

    @Column(name = "feed_reserve_males")
    private double feedReserveMales;

    @OneToOne(mappedBy = "silo", cascade = CascadeType.ALL)
    private Entry entry;


    public Silo(double feedReserveFemales, double feedReserveMales) {
        this.feedReserveFemales = feedReserveFemales;
        this.feedReserveMales = feedReserveMales;
    }

    public Silo() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getFeedReserveFemales() {
        return feedReserveFemales;
    }

    public void setFeedReserveFemales(double feedReserveFemales) {
        this.feedReserveFemales = feedReserveFemales;
    }

    public double getFeedReserveMales() {
        return feedReserveMales;
    }

    public void setFeedReserveMales(double feedReserveMales) {
        this.feedReserveMales = feedReserveMales;
    }

    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }

}
