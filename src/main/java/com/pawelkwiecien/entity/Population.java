package com.pawelkwiecien.entity;

import javax.persistence.*;

@Entity
@Table(name = "population")
public class Population {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "females_total")
    private int femalesTotal;

    @Column(name = "females_dead")
    private int femalesDead;

    @Column(name = "females_removed")
    private int femalesRemoved;

    @Column(name = "males_total")
    private int malesTotal;

    @Column(name = "males_dead")
    private int malesDead;

    @Column(name = "males_removed")
    private int malesRemoved;

    @OneToOne(mappedBy = "population", cascade = CascadeType.ALL)
    private Entry entry;


    public Population(int femalesTotal, int femalesDead, int femalesRemoved, int malesTotal, int malesDead, int malesRemoved) {
        this.femalesTotal = femalesTotal;
        this.femalesDead = femalesDead;
        this.femalesRemoved = femalesRemoved;
        this.malesTotal = malesTotal;
        this.malesDead = malesDead;
        this.malesRemoved = malesRemoved;
    }

    public Population() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFemalesTotal() {
        return femalesTotal;
    }

    public void setFemalesTotal(int femalesTotal) {
        this.femalesTotal = femalesTotal;
    }

    public int getFemalesDead() {
        return femalesDead;
    }

    public void setFemalesDead(int femalesDead) {
        this.femalesDead = femalesDead;
    }

    public int getFemalesRemoved() {
        return femalesRemoved;
    }

    public void setFemalesRemoved(int femalesRemoved) {
        this.femalesRemoved = femalesRemoved;
    }

    public int getMalesTotal() {
        return malesTotal;
    }

    public void setMalesTotal(int malesTotal) {
        this.malesTotal = malesTotal;
    }

    public int getMalesDead() {
        return malesDead;
    }

    public void setMalesDead(int malesDead) {
        this.malesDead = malesDead;
    }

    public int getMalesRemoved() {
        return malesRemoved;
    }

    public void setMalesRemoved(int malesRemoved) {
        this.malesRemoved = malesRemoved;
    }

    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }

}
