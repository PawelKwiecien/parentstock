package com.pawelkwiecien.entity;

import javax.persistence.*;

@Entity
@Table(name = "feed")
public class Feed {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "feed_dosage_females")
    private double feedDosageFemales;

    @Column(name = "feed_intake_females")
    private double feedIntakeFemales;

    @Column(name = "feed_dosage_males")
    private double feedDosageMales;

    @Column(name = "feed_intake_males")
    private double feedIntakeMales;

    @OneToOne(mappedBy = "feed", cascade = CascadeType.ALL)
    private Entry entry;


    public Feed(double feedDosageFemales, double feedIntakeFemales, double feedDosageMales, double feedIntakeMales) {
        this.feedDosageFemales = feedDosageFemales;
        this.feedIntakeFemales = feedIntakeFemales;
        this.feedDosageMales = feedDosageMales;
        this.feedIntakeMales = feedIntakeMales;
    }

    public Feed() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getFeedDosageFemales() {
        return feedDosageFemales;
    }

    public void setFeedDosageFemales(double feedDosageFemales) {
        this.feedDosageFemales = feedDosageFemales;
    }

    public double getFeedIntakeFemales() {
        return feedIntakeFemales;
    }

    public void setFeedIntakeFemales(double feedIntakeFemales) {
        this.feedIntakeFemales = feedIntakeFemales;
    }

    public double getFeedDosageMales() {
        return feedDosageMales;
    }

    public void setFeedDosageMales(double feedDosageMales) {
        this.feedDosageMales = feedDosageMales;
    }

    public double getFeedIntakeMales() {
        return feedIntakeMales;
    }

    public void setFeedIntakeMales(double feedIntakeMales) {
        this.feedIntakeMales = feedIntakeMales;
    }

    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }

}
