package com.pawelkwiecien.entity;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "entry")
public class Entry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "house_id")
    private int houseId;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "age_days")
    private int ageDays;

    @Column(name = "age_weeks")
    private int ageWeeks;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "population_id")
    private Population population;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "feed_id")
    private Feed feed;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "eggs_id")
    private Eggs eggs;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "silo_id")
    private Silo silo;


    public Entry(int houseId, LocalDate date, int ageDays, int ageWeeks, Population population, Feed feed, Eggs eggs, Silo silo) {
        this.houseId = houseId;
        this.date = date;
        this.ageDays = ageDays;
        this.ageWeeks = ageWeeks;
        this.population = population;
        this.feed = feed;
        this.eggs = eggs;
        this.silo = silo;
    }

    public Entry() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHouseId() {
        return houseId;
    }

    public void setHouseId(int houseId) {
        this.houseId = houseId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getAgeDays() {
        return ageDays;
    }

    public void setAgeDays(int ageDays) {
        this.ageDays = ageDays;
    }

    public int getAgeWeeks() {
        return ageWeeks;
    }

    public void setAgeWeeks(int ageWeeks) {
        this.ageWeeks = ageWeeks;
    }

    public Population getPopulation() {
        return population;
    }

    public void setPopulation(Population population) {
        this.population = population;
    }

    public Feed getFeed() {
        return feed;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }

    public Eggs getEggs() {
        return eggs;
    }

    public void setEggs(Eggs eggs) {
        this.eggs = eggs;
    }

    public Silo getSilo() {
        return silo;
    }

    public void setSilo(Silo silo) {
        this.silo = silo;
    }

}
