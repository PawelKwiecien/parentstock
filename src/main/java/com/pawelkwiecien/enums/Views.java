package com.pawelkwiecien.enums;

public enum Views {

    EGGS("/views/Eggs.fxml"),
    POPULATION("/views/Population.fxml"),
    FEED("/views/Feed.fxml"),
    WEIGHT("/views/Weight.fxml"),
    SUBMIT("/views/Submit.fxml");

    private String view;

    Views(String view) {
        this.view = view;
    }

    public String getView() {
        return view;
    }

}
