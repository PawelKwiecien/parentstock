package com.pawelkwiecien.enums;

public enum AlertMessages {

    PROGRAM_ERROR("PROGRAM ERROR"),
    FXL_NOT_FOUND("Could not locate FXML file."),
    INVALID_DATA("INVALID DATA"),
    DATA_INFO("Invalid or no data entered.\nOnly positive numbers are allowed.\n" +
            "Feed entries and Egg Weight are allowed one decimal place.\n" +
            "Birds dead and removed cannot be higher than total number of birds.\n" +
            "Feed intake cannot be higher than feed reserve.\n" +
            "Eggs total number cannot be higher than number of females total.");

private String string;

    AlertMessages(String string) {
        this.string = string;
    }

    public String getString() {
        return string;
    }
}
