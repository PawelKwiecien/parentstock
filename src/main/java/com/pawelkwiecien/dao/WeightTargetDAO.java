package com.pawelkwiecien.dao;

import com.pawelkwiecien.Main;
import com.pawelkwiecien.entity.WeightTarget;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class WeightTargetDAO {

    private SessionFactory factory;
    private Session session;

    public WeightTargetDAO() {
        factory = Main.getFactory();
    }

    public List<WeightTarget> getEntryList() {
        try {
            session = factory.getCurrentSession();
            session.beginTransaction();
            return session.createQuery("FROM WeightTarget", WeightTarget.class).getResultList();
        } finally {
            session.close();
        }
    }

    public WeightTarget getEntry(int id) {
        try {
            session = factory.getCurrentSession();
            session.beginTransaction();
            return session.get(WeightTarget.class, id);
        } finally {
            session.close();
        }
    }
}