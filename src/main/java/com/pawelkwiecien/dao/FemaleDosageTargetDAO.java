package com.pawelkwiecien.dao;

import com.pawelkwiecien.Main;
import com.pawelkwiecien.entity.FemaleDosageTarget;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class FemaleDosageTargetDAO {

    private SessionFactory factory;
    private Session session;

    public FemaleDosageTargetDAO() {
        factory = Main.getFactory();
    }

    public List<FemaleDosageTarget> getEntryList() {
        try {
            session = factory.getCurrentSession();
            session.beginTransaction();
            return session.createQuery("FROM FemaleDosageTarget", FemaleDosageTarget.class).getResultList();
        } finally {
            session.close();
        }
    }

    public FemaleDosageTarget getEntry(int id) {
        try {
            session = factory.getCurrentSession();
            session.beginTransaction();
            return session.get(FemaleDosageTarget.class, id);
        } finally {
            session.close();
        }
    }

}
