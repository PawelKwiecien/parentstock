package com.pawelkwiecien.dao;

import com.pawelkwiecien.Main;
import com.pawelkwiecien.entity.Entry;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;

public class EntryDAO {

    private SessionFactory factory;
    private Session session;

    public EntryDAO() {
        factory = Main.getFactory();
    }

    public List<Entry> getEntryList(int houseNumber) {
        try {
            session = factory.getCurrentSession();
            session.beginTransaction();
            Query<Entry> query = session.createQuery("FROM Entry WHERE houseId =:num", Entry.class);
            query.setParameter("num", houseNumber);
            return query.getResultList();
        } finally {
            session.close();
        }
    }

    public void saveEntry(Entry newEntry) {
        try {
            session = factory.getCurrentSession();
            session.beginTransaction();
            session.save(newEntry);
            session.getTransaction().commit();
        } finally {
            session.close();
        }
    }

    public Entry getLastEntry(int houseNumber) {
        try {
            session = factory.getCurrentSession();
            session.beginTransaction();
            Query query = session.createNativeQuery("SELECT max(id) FROM entry WHERE house_id =:num");
            query.setParameter("num", houseNumber);
            int id = (int) query.getSingleResult();
            return session.get(Entry.class, id);
        } finally {
            session.close();
        }
    }

}
