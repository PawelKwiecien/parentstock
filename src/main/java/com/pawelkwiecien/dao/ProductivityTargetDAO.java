package com.pawelkwiecien.dao;

import com.pawelkwiecien.Main;
import com.pawelkwiecien.entity.ProductivityTarget;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class ProductivityTargetDAO {

    private SessionFactory factory;
    private Session session;


    public ProductivityTargetDAO() {
        factory = Main.getFactory();
    }

    public List<ProductivityTarget> getEntryList() {
        try {
            session = factory.getCurrentSession();
            session.beginTransaction();
            return session.createQuery("FROM ProductivityTarget", ProductivityTarget.class).getResultList();
        } finally {
            session.close();
        }
    }

    public ProductivityTarget getEntry(int id) {
        try {
            session = factory.getCurrentSession();
            session.beginTransaction();
            return session.get(ProductivityTarget.class, id);
        } finally {
            session.close();
        }
    }
}
