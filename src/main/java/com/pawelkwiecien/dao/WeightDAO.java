package com.pawelkwiecien.dao;

import com.pawelkwiecien.Main;
import com.pawelkwiecien.entity.Weight;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import java.util.List;

public class WeightDAO {

    private SessionFactory factory;
    private Session session;

    public WeightDAO() {
        factory = Main.getFactory();
    }

    public List<Weight> getEntryList(int houseNumber) {
        try {
            session = factory.getCurrentSession();
            session.beginTransaction();
            Query<Weight> query = session.createQuery("FROM Weight WHERE houseId =:num", Weight.class);
            query.setParameter("num", houseNumber);
            return query.getResultList();
        } finally {
            session.close();
        }
    }

    public Weight getLastEntry(int houseNumber) {
        try {
            session = factory.getCurrentSession();
            session.beginTransaction();
            Query query = session.createNativeQuery("SELECT max(id) FROM weight WHERE house_id =:num");
            query.setParameter("num", houseNumber);
            int id = (int) query.getSingleResult();
            return session.get(Weight.class, id);
        } finally {
            session.close();
        }
    }

}
