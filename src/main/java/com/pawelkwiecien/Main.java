package com.pawelkwiecien;

import com.pawelkwiecien.entity.*;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class Main extends Application {

    private static SessionFactory factory = new Configuration()
            .configure()
            .addAnnotatedClass(Entry.class)
            .addAnnotatedClass(Population.class)
            .addAnnotatedClass(Feed.class)
            .addAnnotatedClass(Eggs.class)
            .addAnnotatedClass(Silo.class)
            .addAnnotatedClass(Weight.class)
            .addAnnotatedClass(ProductivityTarget.class)
            .addAnnotatedClass(WeightTarget.class)
            .addAnnotatedClass(FemaleDosageTarget.class)
            .buildSessionFactory();


    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/views/Eggs.fxml"));
        primaryStage.setTitle("Parentstock");
        Scene scene = new Scene(root, 1024, 768);
        primaryStage.setScene(scene);
        scene.getStylesheets().add("/style.css");
        primaryStage.setResizable(false);
        primaryStage.show();
    }


    public static void main(String[] args) {
        try {
            launch(args);
        } finally {
            factory.close();
        }

    }

    public static SessionFactory getFactory() {
        return factory;
    }
}
