package com.pawelkwiecien.controller;

import com.pawelkwiecien.dao.EntryDAO;
import com.pawelkwiecien.entity.Entry;
import com.pawelkwiecien.enums.Views;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

public class PopulationController implements Initializable, Controller {

    private EntryDAO entryDAO = new EntryDAO();
    @FXML
    private AnchorPane rootPane;
    @FXML
    private ImageView logo;
    @FXML
    private ChoiceBox<String> houseNumber;
    @FXML
    private TableView<Entry> table;
    @FXML
    private TableColumn<Entry, LocalDate> date;
    @FXML
    private TableColumn<Entry, Integer> femalesDead;
    @FXML
    private TableColumn<Entry, Integer> femalesRemoved;
    @FXML
    private TableColumn<Entry, Integer> femalesTotal;
    @FXML
    private TableColumn<Entry, Integer> malesDead;
    @FXML
    private TableColumn<Entry, Integer> malesRemoved;
    @FXML
    private TableColumn<Entry, Integer> malesTotal;
    @FXML
    private LineChart<Integer, Integer> populationChart;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        File file = new File("src/main/resources/logo.png");
        Image image = new Image(file.toURI().toString());
        logo.setImage(image);
        date.setCellValueFactory(new PropertyValueFactory<>("date"));
        femalesDead.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getPopulation().getFemalesDead()));
        femalesRemoved.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getPopulation().getFemalesRemoved()));
        femalesTotal.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getPopulation().getFemalesTotal()));
        malesDead.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getPopulation().getMalesDead()));
        malesRemoved.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getPopulation().getMalesRemoved()));
        malesTotal.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getPopulation().getMalesTotal()));
        updatePopulation();
    }

    @FXML
    private void updatePopulation() {
        List<Entry> databaseEntries = entryDAO.getEntryList(parseHouseNumber(houseNumber));
        XYChart.Series<Integer, Integer> target = new XYChart.Series<>();
        XYChart.Series<Integer, Integer> chartEntries = new XYChart.Series<>();
        table.getItems().setAll(databaseEntries);
        populationChart.getData().clear();
        populationChart.setCreateSymbols(false);
        populationChart.setLegendVisible(false);
        for (Entry entry : databaseEntries) {
            chartEntries.getData().add(new XYChart.Data<>(entry.getAgeDays(), entry.getPopulation().getFemalesDead()));
        }
        int day = 155;
        while (day <= 432) {
            target.getData().add(new XYChart.Data<>(day++, 5));
        }
        populationChart.getData().add(target);
        populationChart.getData().add(chartEntries);
    }

    @FXML
    @Override
    public void loadEggs() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.EGGS.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

    @FXML
    @Override
    public void loadPopulation() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.POPULATION.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

    @FXML
    @Override
    public void loadFeed() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.FEED.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

    @FXML
    @Override
    public void loadWeight() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.WEIGHT.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

    @FXML
    @Override
    public void loadSubmit() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.SUBMIT.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

}
