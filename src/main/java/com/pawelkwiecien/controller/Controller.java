package com.pawelkwiecien.controller;

import com.pawelkwiecien.enums.AlertMessages;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;

public interface Controller {

    default int parseHouseNumber(ChoiceBox<String> choiceBox) {
        return Integer.parseInt(String.valueOf(choiceBox.getValue().charAt(choiceBox.getValue().length() - 1)));
    }

    default void alertMissingFXML() {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle(AlertMessages.PROGRAM_ERROR.getString());
            alert.setHeaderText(AlertMessages.FXL_NOT_FOUND.getString());
            alert.showAndWait();
    }

    void loadEggs();

    void loadPopulation();

    void loadFeed();

    void loadWeight();

    void loadSubmit();

}
