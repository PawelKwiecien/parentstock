package com.pawelkwiecien.controller;

import com.pawelkwiecien.dao.EntryDAO;
import com.pawelkwiecien.entity.*;
import com.pawelkwiecien.enums.Views;
import com.pawelkwiecien.validation.Validation;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class SubmitController implements Initializable, Controller {

    private EntryDAO entryDAO = new EntryDAO();
    private Validation entryValidation = new Validation();
    @FXML
    private AnchorPane rootPane;
    @FXML
    private ImageView logo;
    @FXML
    private ChoiceBox<String> houseNumber;
    @FXML
    private Text date;
    @FXML
    private TextField femalesDead;
    @FXML
    private TextField malesDead;
    @FXML
    private TextField femalesRemoved;
    @FXML
    private TextField malesRemoved;
    @FXML
    private TextField femaleDosage;
    @FXML
    private TextField maleDosage;
    @FXML
    private TextField femaleFeed;
    @FXML
    private TextField maleFeed;
    @FXML
    private Text feedReserveFemales;
    @FXML
    private Text feedReserveMales;
    @FXML
    private TextField incubationEggs;
    @FXML
    private TextField misshapenEggs;
    @FXML
    private TextField doubleYolkEggs;
    @FXML
    private TextField crackedEggs;
    @FXML
    private TextField eggsWeight;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        File file = new File("src/main/resources/logo.png");
        Image image = new Image(file.toURI().toString());
        logo.setImage(image);
        updateSubmitForm();
    }

    @FXML
    private void updateSubmitForm() {
        Entry lastEntry = entryDAO.getLastEntry(parseHouseNumber(houseNumber));
        date.setText(lastEntry.getDate().plusDays(2).toString());
        feedReserveFemales.setText(String.valueOf(lastEntry.getSilo().getFeedReserveFemales()));
        feedReserveMales.setText(String.valueOf(lastEntry.getSilo().getFeedReserveMales()));
    }

    @FXML
    private void clearButtonPressed() {
        femalesDead.setText(null);
        femalesRemoved.setText(null);
        malesDead.setText(null);
        malesRemoved.setText(null);
        femaleDosage.setText(null);
        femaleFeed.setText(null);
        maleDosage.setText(null);
        maleFeed.setText(null);
        incubationEggs.setText(null);
        misshapenEggs.setText(null);
        doubleYolkEggs.setText(null);
        crackedEggs.setText(null);
        eggsWeight.setText(null);
    }

    @FXML
    public void submitButtonPressed() {
        if (entryValidation.validateEntryFields(
                femalesDead,
                femalesRemoved,
                malesDead,
                malesRemoved,
                femaleDosage,
                femaleFeed,
                maleDosage,
                maleFeed,
                incubationEggs,
                misshapenEggs,
                doubleYolkEggs,
                crackedEggs,
                eggsWeight,
                parseHouseNumber(houseNumber)
        )) {
            entryDAO.saveEntry(createEntryFromFields());
            updateSubmitForm();
            clearButtonPressed();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("DONE");
            alert.setHeaderText("Entry successfully added to House #" + parseHouseNumber(houseNumber));
            alert.showAndWait();
        }
    }

    private Entry createEntryFromFields() {
        int submittedHouseNumber = parseHouseNumber(houseNumber);
        Entry lastEntry = entryDAO.getLastEntry(submittedHouseNumber);

        Population populationEntry = new Population(
                (lastEntry.getPopulation().getFemalesTotal() -
                        lastEntry.getPopulation().getFemalesDead()) -
                        lastEntry.getPopulation().getFemalesRemoved(),
                Integer.parseInt(femalesDead.getText()),
                Integer.parseInt(femalesRemoved.getText()),
                (lastEntry.getPopulation().getMalesTotal() -
                        Integer.parseInt(malesDead.getText()) -
                        Integer.parseInt(malesRemoved.getText())),
                Integer.parseInt(malesDead.getText()),
                Integer.parseInt(malesRemoved.getText()));

        Feed feedEntry = new Feed(
                Double.parseDouble(femaleDosage.getText()),
                Double.parseDouble(femaleFeed.getText()),
                Double.parseDouble(maleDosage.getText()),
                Double.parseDouble(maleFeed.getText()));

        Eggs eggsEntry = new Eggs(
                (Integer.parseInt(incubationEggs.getText())) +
                        Integer.parseInt(misshapenEggs.getText()) +
                        Integer.parseInt(doubleYolkEggs.getText()) +
                        Integer.parseInt(crackedEggs.getText()),
                Integer.parseInt(incubationEggs.getText()),
                Integer.parseInt(misshapenEggs.getText()),
                Integer.parseInt(doubleYolkEggs.getText()),
                Integer.parseInt(crackedEggs.getText()),
                Double.parseDouble(eggsWeight.getText()));

        Silo siloEntry = new Silo(
                lastEntry.getSilo().getFeedReserveFemales() - Double.parseDouble(femaleFeed.getText()),
                lastEntry.getSilo().getFeedReserveMales() - Double.parseDouble(maleFeed.getText()));

        return new Entry(submittedHouseNumber, lastEntry.getDate().plusDays(2), lastEntry.getAgeDays() + 1,
                Math.floorDiv(lastEntry.getAgeDays() + 1, 7) + 1, populationEntry, feedEntry, eggsEntry, siloEntry);
    }

    @FXML
    @Override
    public void loadEggs() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.EGGS.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

    @FXML
    @Override
    public void loadPopulation() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.POPULATION.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

    @FXML
    @Override
    public void loadFeed() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.FEED.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

    @FXML
    @Override
    public void loadWeight() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.WEIGHT.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

    @FXML
    @Override
    public void loadSubmit() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.SUBMIT.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }
}
