package com.pawelkwiecien.controller;

import com.pawelkwiecien.dao.EntryDAO;
import com.pawelkwiecien.dao.ProductivityTargetDAO;
import com.pawelkwiecien.entity.Entry;
import com.pawelkwiecien.entity.ProductivityTarget;
import com.pawelkwiecien.enums.Views;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

public class EggsController implements Initializable, Controller {

    private EntryDAO entryDAO = new EntryDAO();
    private ProductivityTargetDAO targetDAO = new ProductivityTargetDAO();
    @FXML
    private AnchorPane rootPane;
    @FXML
    private ImageView logo;
    @FXML
    private ChoiceBox<String> houseNumber;
    @FXML
    private TableView<Entry> table;
    @FXML
    private TableColumn<Entry, LocalDate> date;
    @FXML
    private TableColumn<Entry, Integer> incubationEggs;
    @FXML
    private TableColumn<Entry, Integer> misshapenEggs;
    @FXML
    private TableColumn<Entry, Integer> doubleYolkEggs;
    @FXML
    private TableColumn<Entry, Integer> crackedEggs;
    @FXML
    private TableColumn<Entry, Integer> totalEggs;
    @FXML
    private TableColumn<Entry, Double> eggsWeight;
    @FXML
    private LineChart<Integer, Double> eggsChart;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        File file = new File("src/main/resources/logo.png");
        Image image = new Image(file.toURI().toString());
        logo.setImage(image);
        date.setCellValueFactory(new PropertyValueFactory<>("date"));
        incubationEggs.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getEggs().getEggsIncubation()));
        misshapenEggs.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getEggs().getEggsMisshapen()));
        doubleYolkEggs.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getEggs().getEggsDoubleYolk()));
        crackedEggs.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getEggs().getEggsCracked()));
        totalEggs.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getEggs().getEggsTotal()));
        eggsWeight.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getEggs().getEggsWeight()));
        updateEggs();
    }

    @FXML
    private void updateEggs() {
        List<Entry> databaseEntries = entryDAO.getEntryList(parseHouseNumber(houseNumber));
        List<ProductivityTarget> productivityTargets = targetDAO.getEntryList();
        XYChart.Series<Integer, Double> targets = new XYChart.Series<>();
        XYChart.Series<Integer, Double> chartEntries = new XYChart.Series<>();
        table.getItems().setAll(databaseEntries);
        eggsChart.getData().clear();
        eggsChart.setCreateSymbols(false);
        eggsChart.setLegendVisible(false);
        for (ProductivityTarget entry : productivityTargets) {
            targets.getData().add(new XYChart.Data<>(entry.getAgeDays(), entry.getTarget()));
        }
        for (Entry entry : databaseEntries) {
            double productivity = (double) (entry.getEggs().getEggsTotal() * 100) / entry.getPopulation().getFemalesTotal();
            chartEntries.getData().add(new XYChart.Data<>(entry.getAgeDays(), productivity));
        }
        eggsChart.getData().add(targets);
        eggsChart.getData().add(chartEntries);
    }

    @FXML
    @Override
    public void loadEggs() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.EGGS.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

    @FXML
    @Override
    public void loadPopulation() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.POPULATION.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

    @FXML
    @Override
    public void loadFeed() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.FEED.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

    @FXML
    @Override
    public void loadWeight() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.WEIGHT.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

    @FXML
    @Override
    public void loadSubmit() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.SUBMIT.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

}
