package com.pawelkwiecien.controller;

import com.pawelkwiecien.dao.EntryDAO;
import com.pawelkwiecien.dao.FemaleDosageTargetDAO;
import com.pawelkwiecien.entity.Entry;
import com.pawelkwiecien.entity.FemaleDosageTarget;
import com.pawelkwiecien.enums.Views;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;

public class FeedController implements Initializable, Controller {

    private EntryDAO entryDAO = new EntryDAO();
    private FemaleDosageTargetDAO dosageTargetDAO = new FemaleDosageTargetDAO();
    @FXML
    private AnchorPane rootPane;
    @FXML
    private ImageView logo;
    @FXML
    private ChoiceBox<String> houseNumber;
    @FXML
    private TableView<Entry> table;
    @FXML
    private TableColumn<Entry, LocalDate> date;
    @FXML
    private TableColumn<Entry, Double> femalesDosage;
    @FXML
    private TableColumn<Entry, Double> femalesIntake;
    @FXML
    private TableColumn<Entry, Double> malesDosage;
    @FXML
    private TableColumn<Entry, Double> malesIntake;
    @FXML
    private LineChart<Integer, Double> feedChart;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        File file = new File("src/main/resources/logo.png");
        Image image = new Image(file.toURI().toString());
        logo.setImage(image);
        date.setCellValueFactory(new PropertyValueFactory<>("date"));
        femalesDosage.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getFeed().getFeedDosageFemales()));
        femalesIntake.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getFeed().getFeedIntakeFemales()));
        malesDosage.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getFeed().getFeedDosageMales()));
        malesIntake.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getFeed().getFeedIntakeMales()));
        updateFeed();
    }

    @FXML
    private void updateFeed() {
        List<Entry> databaseEntries = entryDAO.getEntryList(parseHouseNumber(houseNumber));
        List<FemaleDosageTarget> dosageTargets = dosageTargetDAO.getEntryList();
        XYChart.Series<Integer, Double> chartEntries = new XYChart.Series<>();
        XYChart.Series<Integer, Double> targets = new XYChart.Series<>();
        table.getItems().setAll(databaseEntries);
        feedChart.getData().clear();
        feedChart.setCreateSymbols(false);
        feedChart.setLegendVisible(false);
        for (Entry entry : databaseEntries) {
            chartEntries.getData().add(new XYChart.Data<>(entry.getAgeDays(), entry.getFeed().getFeedDosageFemales()));
        }
        for (FemaleDosageTarget target : dosageTargets) {
            targets.getData().add(new XYChart.Data<>(target.getAgeDays(), target.getTarget()));
        }
        feedChart.getData().add(targets);
        feedChart.getData().add(chartEntries);
    }

    @FXML
    @Override
    public void loadEggs() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.EGGS.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

    @FXML
    @Override
    public void loadPopulation() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.POPULATION.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

    @FXML
    @Override
    public void loadFeed() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.FEED.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

    @FXML
    @Override
    public void loadWeight() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.WEIGHT.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

    @FXML
    @Override
    public void loadSubmit() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.SUBMIT.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

}
