package com.pawelkwiecien.controller;

import com.pawelkwiecien.dao.WeightDAO;
import com.pawelkwiecien.dao.WeightTargetDAO;
import com.pawelkwiecien.entity.Weight;
import com.pawelkwiecien.entity.WeightTarget;
import com.pawelkwiecien.enums.Views;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class WeightController implements Initializable, Controller {

    private WeightDAO weightDAO = new WeightDAO();
    private WeightTargetDAO targetDAO = new WeightTargetDAO();
    @FXML
    private AnchorPane rootPane;
    @FXML
    private ImageView logo;
    @FXML
    private ChoiceBox<String> houseNumber;
    @FXML
    private TableView<Weight> table;
    @FXML
    private TableColumn<Weight, Integer> week;
    @FXML
    private TableColumn<Weight, Integer> femalesWeight;
    @FXML
    private TableColumn<Weight, Integer> malesWeight;
    @FXML
    private LineChart<Integer, Integer> weightChart;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        File file = new File("src/main/resources/logo.png");
        Image image = new Image(file.toURI().toString());
        logo.setImage(image);
        week.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getWeekNumber()));
        femalesWeight.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getWeightFemales()));
        malesWeight.setCellValueFactory(param -> new SimpleObjectProperty<>(param.getValue().getWeightMales()));
        updateWeight();
    }

    @FXML
    private void updateWeight() {
        List<Weight> databaseEntries = weightDAO.getEntryList(parseHouseNumber(houseNumber));
        List<WeightTarget> weightTargets = targetDAO.getEntryList();
        XYChart.Series<Integer, Integer> targets = new XYChart.Series<>();
        XYChart.Series<Integer, Integer> chartEntries = new XYChart.Series<>();
        table.getItems().setAll(databaseEntries);
        weightChart.getData().clear();
        weightChart.setCreateSymbols(false);
        weightChart.setLegendVisible(false);
        for (WeightTarget entry : weightTargets) {
            targets.getData().add(new XYChart.Data<>(entry.getAgeWeek(), entry.getTarget()));
        }
        for (Weight entry : databaseEntries) {
            chartEntries.getData().add(new XYChart.Data<>(entry.getWeekNumber(), entry.getWeightFemales()));
        }
        weightChart.getData().add(targets);
        weightChart.getData().add(chartEntries);
    }

    @FXML
    @Override
    public void loadEggs() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.EGGS.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

    @FXML
    @Override
    public void loadPopulation() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.POPULATION.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

    @FXML
    @Override
    public void loadFeed() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.FEED.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

    @FXML
    @Override
    public void loadWeight() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.WEIGHT.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

    @FXML
    @Override
    public void loadSubmit() {
        try {
            rootPane.getChildren().setAll((AnchorPane) FXMLLoader.load(getClass().getResource(Views.SUBMIT.getView())));
        } catch (IOException e) {
            alertMissingFXML();
        }
    }

}
