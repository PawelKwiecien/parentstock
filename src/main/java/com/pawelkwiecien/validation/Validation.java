package com.pawelkwiecien.validation;

import com.pawelkwiecien.enums.AlertMessages;
import com.pawelkwiecien.dao.EntryDAO;
import com.pawelkwiecien.entity.Entry;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;

public class Validation {

    private EntryDAO dao = new EntryDAO();

    public boolean validateEntryFields(
            TextField femalesDead,
            TextField femalesRemoved,
            TextField malesDead,
            TextField malesRemoved,
            TextField dosageFemales,
            TextField feedFemales,
            TextField dosageMales,
            TextField feedMales,
            TextField eggsIncubation,
            TextField eggsMisshapen,
            TextField eggsDoubleYolk,
            TextField eggsCracked,
            TextField eggsWeight,
            int houseNumber
    ) {
        if (validateFemalesDeadOrRemoved(femalesDead, femalesRemoved, houseNumber) &&
                validateMalesDeadOrRemoved(malesDead, malesRemoved, houseNumber) &&
                validateFeedIntakeFemales(feedFemales, houseNumber) &&
                validateFeedIntakeMales(feedMales, houseNumber) &&
                validateFeedDosage(dosageFemales, dosageMales) &&
                validateEggNumbers(eggsIncubation, eggsMisshapen, eggsDoubleYolk, eggsCracked, houseNumber) &&
                validateEggsWeight(eggsWeight)) {
            return true;
        } else {
            displayAlert();
            return false;
        }

    }

    private boolean isValidDouble(TextField input) {
        if (input.getText().matches("[0-9]+(\\.[0-9])?|9(.0)?")) {
            return true;
        } else {
            return input.getText().matches("[0-9]+(\\.[0-9])?|9(.0)?");
        }
    }

    private boolean isValidInteger(TextField input) {
        return input.getText().matches("[0-9]+");
    }

    private boolean validateFemalesDeadOrRemoved(TextField femalesDead, TextField femalesRemoved, int houseNumber) {
        if (isValidInteger(femalesDead) && isValidInteger(femalesRemoved)) {
            Entry lastEntry = dao.getLastEntry(houseNumber);
            int femalesDeadAndRemoved = Integer.parseInt(femalesDead.getText()) + Integer.parseInt(femalesRemoved.getText());
            int femalesTotal = lastEntry.getPopulation().getFemalesTotal() - lastEntry.getPopulation().getFemalesDead() - lastEntry.getPopulation().getFemalesRemoved();
            return femalesDeadAndRemoved <= femalesTotal;
        }
        return false;
    }

    private boolean validateMalesDeadOrRemoved(TextField malesDead, TextField malesRemoved, int houseNumber) {
        if (isValidInteger(malesDead) && isValidInteger(malesRemoved)) {
            Entry lastEntry = dao.getLastEntry(houseNumber);
            int malesDeadOrRemoved = Integer.parseInt(malesDead.getText()) + Integer.parseInt(malesRemoved.getText());
            int malesTotal = lastEntry.getPopulation().getMalesTotal() - lastEntry.getPopulation().getMalesDead() - lastEntry.getPopulation().getMalesRemoved();
            return malesDeadOrRemoved <= malesTotal;
        }
        return false;
    }

    private boolean validateFeedIntakeFemales(TextField textField, int houseNumber) {
        if (isValidDouble(textField)) {
            Entry lastEntry = dao.getLastEntry(houseNumber);
            return Double.parseDouble(textField.getText()) <= lastEntry.getSilo().getFeedReserveFemales();
        }
        return false;
    }

    private boolean validateFeedIntakeMales(TextField textField, int houseNumber) {
        if (isValidDouble(textField)) {
            Entry lastEntry = dao.getLastEntry(houseNumber);
            return Double.parseDouble(textField.getText()) <= lastEntry.getSilo().getFeedReserveMales();
        }
        return false;
    }

    private boolean validateFeedDosage(TextField dosageFemales, TextField dosageMales) {
        return isValidDouble(dosageFemales) && isValidDouble(dosageMales);
    }

    private boolean validateEggNumbers(TextField eggsIncubation, TextField eggsMisshapen, TextField eggsDoubleYolk, TextField eggsCracked, int houseNumber) {
        if (isValidInteger(eggsIncubation) && isValidInteger(eggsMisshapen) && isValidInteger(eggsDoubleYolk) && isValidInteger(eggsCracked)) {
            Entry previousEntry = dao.getLastEntry(houseNumber);
            int totalEggs = Integer.parseInt(eggsIncubation.getText()) + Integer.parseInt(eggsMisshapen.getText()) + Integer.parseInt(eggsDoubleYolk.getText()) + Integer.parseInt(eggsCracked.getText());
            int totalFemales = (previousEntry.getPopulation().getFemalesTotal() - previousEntry.getPopulation().getFemalesDead() - previousEntry.getPopulation().getFemalesRemoved());
            return totalEggs <= totalFemales;
        }
        return false;
    }

    private boolean validateEggsWeight(TextField textField) {
        return isValidDouble(textField);
    }

    private void displayAlert() {
        Alert alert = new Alert((Alert.AlertType.ERROR));
        alert.setTitle(AlertMessages.INVALID_DATA.getString());
        alert.setHeaderText(AlertMessages.DATA_INFO.getString());
        alert.showAndWait();
    }


}
